package org.quantrserver;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HibernateUtil {
	private static final Logger logger = LoggerFactory.getLogger(HibernateUtil.class);
	private static final SessionFactory sessionFactory = buildSessionFactory();
	private static ServiceRegistry serviceRegistry;

	public static void main(String args[]) {
		Session session = HibernateUtil.openSession();
		session.close();
		HibernateUtil.getSessionFactory().close();
	}

	private static SessionFactory buildSessionFactory() {
		try {
			Configuration config = new Configuration().configure("hibernate.cfg.xml");

			List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
			classLoadersList.add(ClasspathHelper.contextClassLoader());
			//classLoadersList.add(ClasspathHelper.staticClassLoader());

			Reflections reflections = new Reflections(new ConfigurationBuilder().setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
					.setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
					.filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("org.quantrserver.table."))));
			Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);
			Iterator<Class<?>> iterator = classes.iterator();
			while (iterator.hasNext()) {
				config.addAnnotatedClass(iterator.next());
			}

			//			Properties props = config.getProperties();
			//			Enumeration e = props.propertyNames();
			//			while (e.hasMoreElements()) {
			//				String key = (String) e.nextElement();
			//				System.out.println(key + " -- " + props.getProperty(key));
			//			}

			serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
			return config.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			logger.error("exception", ex);

			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static Session openSession() {
		return getSessionFactory().openSession();
	}

	public static StatelessSession openStatelessSession() {
		return getSessionFactory().openStatelessSession();
	}

	public static void shutdown() {
		getSessionFactory().close();
	}

}