package org.quantrserver.table;

import static javax.persistence.GenerationType.IDENTITY;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer id;

	public String owner;
	public String gameType;
	public int depositAmount;
	public int cutOffAmount;
	public String usernameLongCurrentMonth;
	public String passwordLongCurrentMonth;
	public String usernameShortCurrentMonth;
	public String passwordShortCurrentMonth;
	public String api1LongCurrentMonth;
	public String api2LongCurrentMonth;
	public String api1ShortCurrentMonth;
	public String api2ShortCurrentMonth;

	public String usernameLongNextMonth;
	public String passwordLongNextMonth;
	public String usernameShortNextMonth;
	public String passwordShortNextMonth;
	public String api1LongNextMonth;
	public String api2LongNextMonth;
	public String api1ShortNextMonth;
	public String api2ShortNextMonth;

	public Account() {

	}

	public Account(String owner, String gameType, int depositAmount, int cutOffAmount, String usernameLongCurrentMonth, String passwordLongCurrentMonth,
			String api1LongCurrentMonth, String api2LongCurrentMonth, String usernameShortCurrentMonth, String passwordShortCurrentMonth, String api1ShortCurrentMonth,
			String api2ShortCurrentMonth, String usernameLongNextMonth, String passwordLongNextMonth, String api1LongNextMonth, String api2LongNextMonth,
			String usernameShortNextMonth, String passwordShortNextMonth, String api1ShortNextMonth, String api2ShortNextMonth) {
		super();
		this.owner = owner;
		this.gameType = gameType;
		this.usernameLongCurrentMonth = usernameLongCurrentMonth;
		this.passwordLongCurrentMonth = passwordLongCurrentMonth;
		this.usernameShortCurrentMonth = usernameShortCurrentMonth;
		this.passwordShortCurrentMonth = passwordShortCurrentMonth;
		this.depositAmount = depositAmount;
		this.cutOffAmount = cutOffAmount;
		this.api1LongCurrentMonth = api1LongCurrentMonth;
		this.api2LongCurrentMonth = api2LongCurrentMonth;
		this.api1ShortCurrentMonth = api1ShortCurrentMonth;
		this.api2ShortCurrentMonth = api2ShortCurrentMonth;

		this.usernameLongNextMonth = usernameLongNextMonth;
		this.passwordLongNextMonth = passwordLongNextMonth;
		this.usernameShortNextMonth = usernameShortNextMonth;
		this.passwordShortNextMonth = passwordShortNextMonth;
		this.depositAmount = depositAmount;
		this.cutOffAmount = cutOffAmount;
		this.api1LongNextMonth = api1LongNextMonth;
		this.api2LongNextMonth = api2LongNextMonth;
		this.api1ShortNextMonth = api1ShortNextMonth;
		this.api2ShortNextMonth = api2ShortNextMonth;
	}

}
