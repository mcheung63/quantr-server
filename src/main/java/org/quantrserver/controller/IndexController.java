package org.quantrserver.controller;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class IndexController {

	@RequestMapping("/")
	public ModelAndView index(ModelAndView model) {
		ModelAndView view = new ModelAndView();
		view.setViewName("index");
		view.addObject("time", new Date());
		return view;
	}

}
