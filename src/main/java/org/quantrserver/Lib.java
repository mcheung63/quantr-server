package org.quantrserver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.SQLGrammarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.peterswing.CommonLib;
import org.quantrserver.table.Account;
import org.quantrserver.table.User;

public class Lib {

	private static Logger logger = LoggerFactory.getLogger(Lib.class);
	public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss S");
	//public static String stockFeedURL = "http://210.5.164.14:30099";
	public static String stockFeedURL = "https://127.0.0.1:8082";

	public static boolean isUserTableEmpty() {
		Session session = HibernateUtil.openSession();
		try {
			List<User> tables = session.createQuery("from User").list();
			return tables.size() == 0;
		} catch (SQLGrammarException ex) {
			return true;
		} catch (Exception ex) {
			logger.error("exception", ex);
			return true;
		} finally {
			session.close();
		}
	}

	public static void initDB() {
		logger.info("initDB()");
		Session session = HibernateUtil.openSession();
		Transaction tx = session.beginTransaction();
		try {
			User admin = new User();
			admin.username = "admin";
			admin.password = "1234";
			admin.enable = true;
			session.save(admin);
			
			Account account = new Account("peter", "index", 1000000, 100000, "p1", "p1 password", null, null, "p2", "p2 password", null, null, "p3", "p3 password", null, null,
					"p4", "p4 password", null, null);
			session.save(account);

			account = new Account("peter", "index", 1000000, 100000, "r1", "r1 password", null, null, "r2", "r2 password", null, null, "r3", "r3 password", null, null, "r4",
					"r4 password", null, null);
			session.save(account);

			tx.commit();
		} catch (Exception e) {
			logger.error(CommonLib.printException(e));
		} finally {
			try {
				session.close();
			} catch (Exception ex) {
				logger.error(CommonLib.printException(ex));
			}
		}
	}

	public static Date toDate(String str) {
		try {
			return sdf.parse(str);
		} catch (ParseException e) {
			return null;
		}
	}
}
